# Модуль "Простые звонки" для asterisk

## Сборка deb пакета

```
$ git clone git@bitbucket.org:staltrans/prostiezvonki.git
$ rm -rf prostiezvonki/{.git,.gitignore,README.md}
$ fakeroot dpkg-deb -b prostiezvonki
```

## Установка deb пакета

```
# dpkg -i prostiezvonki.deb
```

## Ссылки

### Asterisk 1.8

[x32](http://www.prostiezvonki.ru/installs/prostiezvonki_asterisk1.8_x86.zip) / [amd64](http://www.prostiezvonki.ru/installs/prostiezvonki_asterisk1.8_x64.zip)

[х32 FreePBX](http://www.prostiezvonki.ru/installs/prostiezvonki_freePBX_asterisk1.8_x86.zip) / [amd64 FreePBX](http://www.prostiezvonki.ru/installs/prostiezvonki_freePBX_asterisk1.8_x64.zip)

### Asterisk 11

[x32](http://www.prostiezvonki.ru/installs/prostiezvonki_asterisk11_x86.zip) / [amd64](http://www.prostiezvonki.ru/installs/prostiezvonki_asterisk11_x64.zip)

[х32 FreePBX](http://www.prostiezvonki.ru/installs/prostiezvonki_freePBX_asterisk11_x86.zip) / [amd64 FreePBX](http://www.prostiezvonki.ru/installs/prostiezvonki_freePBX_asterisk11_x64.zip)

### Asterisk 13

[x32](http://www.prostiezvonki.ru/installs/prostiezvonki_asterisk13_x86.zip) / [amd64](http://www.prostiezvonki.ru/installs/prostiezvonki_asterisk13_x64.zip)

[х32 FreePBX](http://www.prostiezvonki.ru/installs/prostiezvonki_freePBX_asterisk13_x86.zip) / [amd64 FreePBX](http://www.prostiezvonki.ru/installs/prostiezvonki_freePBX_asterisk13_x64.zip)

